#pragma once
#include "afxwin.h"

// CLicenseDlg dialog

class CLicenseDlg : public CDialog
{
	DECLARE_DYNAMIC(CLicenseDlg)

public:
	CLicenseDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CLicenseDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG1 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	CString m_csPath;
	CString m_csId;
	afx_msg void OnBnClickedButtonDirchoose();
	afx_msg void OnBnClickedButtonGetid();
	CString m_csStatus;
	BOOL m_bOk;
	afx_msg void OnBnClickedButtonApplyCodes();
	CString m_csCodes;
	CListBox m_clbHwids;
	afx_msg void OnBnClickedButtonCopyHwid();
};
