#pragma once
#include "afxcmn.h"


// CLicenseDlg2 dialog
typedef struct _tagLICENSE
{
	CString csModID;		// module id
	CString csModName;		// module name
	int nType;			// 1 = User, 2 = Computer, 3 = Counter, 4 = Yes/No, 5 = Item, 6 = Concurrent User (only possible if the necessary licence exists)
	int nLicenses;		// Number of users or computers etc. In a Yes/No-Module 1 for yes and 0 for no.
	BOOL bDemo;			// demo?
	DATE dtExpires;		// Date when this module will expire, 0 for unlimited
	int nNoOfDays;		// Number of days this module will run after the first usage
	DATE dtMaxDate;		// Max date when this module will no longer work, even if there are open days, 0 for unlimited
	CString csTag;		// The tag value of that module
	BOOL bAllowDeactivate;	// Can users, computers or items be deactivated In that module? 
	int nWebActivation;	// WebActivation State of the new module (0-3). See Web Activation Documentation. 

	BOOL bYesNo;		// TRUE = yes, FALSE = no
	int nRemDays;		// amount of days left
	int nRemLic;		// amount of licenses left
} LICENSE;

class CLicenseDlg2 : public CDialog
{
	DECLARE_DYNAMIC(CLicenseDlg2)

public:
	CLicenseDlg2(CWnd* pParent = NULL);   // standard constructor
	virtual ~CLicenseDlg2();

// Dialog Data
	enum { IDD = IDD_DIALOG2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonDirchoose();
	afx_msg void OnBnClickedButtonGetid();
	CString m_csLicfilePath;
	CString m_csHwid;
	CString m_csStatus;
	CListCtrl m_clLicenses;
	afx_msg void OnBnClickedButtonShowLic();
};
