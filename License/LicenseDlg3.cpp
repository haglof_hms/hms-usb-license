// LicenseDlg3.cpp : implementation file
//

#include "stdafx.h"
#include "License.h"
#include "LicenseDlg3.h"
#include "XBrowseForFolder.h"


// CLicenseDlg3 dialog

IMPLEMENT_DYNAMIC(CLicenseDlg3, CDialog)

CLicenseDlg3::CLicenseDlg3(CWnd* pParent /*=NULL*/)
	: CDialog(CLicenseDlg3::IDD, pParent)
	, m_csPath(_T(""))
	, m_csKey(_T(""))
{

}

CLicenseDlg3::~CLicenseDlg3()
{
}

void CLicenseDlg3::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_PATH, m_csPath);
	DDX_Text(pDX, IDC_EDIT2, m_csKey);
}


BEGIN_MESSAGE_MAP(CLicenseDlg3, CDialog)
	ON_BN_CLICKED(IDC_BUTTON1, &CLicenseDlg3::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON_DIRCHOOSE, &CLicenseDlg3::OnBnClickedButtonDirchoose)
END_MESSAGE_MAP()


// CLicenseDlg3 message handlers

void CLicenseDlg3::OnBnClickedButton1()
{
	UpdateData(TRUE);

	// l�gg till aktiveringsnyckeln till licensfilen
	long lRes = theApp.proxyLicProtector.Prepare(m_csPath + _T("HaglofManagementSystems.lic"), _T("1234567890"));
	if(lRes != 0)
	{
		AfxMessageBox(theApp.proxyLicProtector.GetErrorMessage(lRes), MB_OK);
		UpdateData(FALSE);
		return;
	}

	lRes = theApp.proxyLicProtector.ApplyActivationKey(m_csKey);
	if(lRes != 0)
	{
		AfxMessageBox(theApp.proxyLicProtector.GetErrorMessage(lRes), MB_OK);
		UpdateData(FALSE);
		return;
	}

	theApp.proxyLicProtector.Quit();
}

void CLicenseDlg3::OnBnClickedButtonDirchoose()
{
	UpdateData(TRUE);

	// display a directory chooser
	TCHAR szFolder[MAX_PATH*2];
	szFolder[0] = _T('\0');
	BOOL bRet = XBrowseForFolder(NULL,
		m_csPath,
		szFolder,
		sizeof(szFolder)/sizeof(TCHAR)-2);

	if(bRet == TRUE)
	{
		m_csPath = szFolder;
		if(m_csPath.GetAt(m_csPath.GetLength()-1) != '\\')
			m_csPath += _T("\\");

		UpdateData(FALSE);
	}
}
