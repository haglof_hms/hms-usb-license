// LicenseDlg.cpp : implementation file
//

#include "stdafx.h"
#include "License.h"
#include "LicenseDlg.h"
#include "XBrowseForFolder.h"
#include "w3c.h"

// CLicenseDlg dialog

IMPLEMENT_DYNAMIC(CLicenseDlg, CDialog)

CLicenseDlg::CLicenseDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLicenseDlg::IDD, pParent)
	, m_csPath(_T(""))
	, m_csId(_T(""))
	, m_csStatus(_T(""))
	, m_csCodes(_T(""))
{

}

CLicenseDlg::~CLicenseDlg()
{
}

void CLicenseDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_PATH, m_csPath);
	DDX_Text(pDX, IDC_STATIC_STATUS, m_csStatus);
	DDX_Text(pDX, IDC_EDIT_CODES, m_csCodes);
	DDX_Control(pDX, IDC_LIST_HWID, m_clbHwids);
}
//	DDX_Text(pDX, IDC_EDIT_ID, m_csId);


BEGIN_MESSAGE_MAP(CLicenseDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_DIRCHOOSE, &CLicenseDlg::OnBnClickedButtonDirchoose)
	ON_BN_CLICKED(IDC_BUTTON_GETID, &CLicenseDlg::OnBnClickedButtonGetid)
	ON_BN_CLICKED(IDC_BUTTON_APPLY_CODES, &CLicenseDlg::OnBnClickedButtonApplyCodes)
	ON_BN_CLICKED(IDC_BUTTON_COPY_HWID, &CLicenseDlg::OnBnClickedButtonCopyHwid)
END_MESSAGE_MAP()


// CLicenseDlg message handlers

void CLicenseDlg::OnBnClickedButtonDirchoose()
{
	UpdateData(TRUE);

	// display a directory chooser
	TCHAR szFolder[MAX_PATH*2];
	szFolder[0] = _T('\0');
	BOOL bRet = XBrowseForFolder(NULL,
		m_csPath,
		szFolder,
		sizeof(szFolder)/sizeof(TCHAR)-2);

	if(bRet == TRUE)
	{
		m_csPath = szFolder;
		if(m_csPath.GetAt(m_csPath.GetLength()-1) != '\\')
			m_csPath += _T("\\");

		UpdateData(FALSE);
	}
}

// retreive the HWID for the USB-memory
void CLicenseDlg::OnBnClickedButtonGetid()
{
	UpdateData(TRUE);

	m_bOk = TRUE;

	m_csStatus = _T(" Getting USB hardware id");
	UpdateData(FALSE);

	// copy a dummy license-file to the usb-memory and open it
	CopyFile(theApp.m_csProgPath + _T("dummy.lic"), m_csPath + _T("dummy.lic"), FALSE);

	// open license-file
	long lRes = theApp.proxyLicProtector.Prepare(m_csPath + _T("dummy.lic"), _T("dummy!"));
	if(lRes != 0)
	{
		AfxMessageBox(theApp.proxyLicProtector.GetErrorMessage(lRes), MB_OK);
		m_csStatus = _T(" Unable to open dummy.lic!");
		UpdateData(FALSE);
		return;
	}

	for(int nLoop=1; nLoop<13; nLoop++)
	{
		m_csId.Format(_T("%02d: %s"), nLoop, theApp.proxyLicProtector.GetInstCode(nLoop));
		m_clbHwids.AddString(m_csId);
	}

	m_csId = theApp.proxyLicProtector.GetInstCode(11);
	if(m_csId == _T(""))
	{
		m_csId = _T("ERROR!");
		m_csStatus = _T(" Could not get USB hardware id!");
		UpdateData(FALSE);
		return;
	}

	theApp.proxyLicProtector.Quit();

	DeleteFile(m_csPath + _T("dummy.lic"));


	m_csStatus = _T(" Sending hardware id to server");
	UpdateData(FALSE);

	// send a request to the webserver to make a license file
	CString csServer, csPage, csBuf;
	W3Client w3;
	int nRet;

	csServer = _T("http://support");
	csPage.Format(_T("/newlicence.asp?cmd=autogen&code=%s"), m_csId);

	if( w3.Connect(csServer) )
	{
		if( w3.Request(csPage) )
		{
			if( w3.QueryResult() == 200 )
			{
				m_csStatus = _T(" Downloading license file");
				UpdateData(FALSE);

				TCHAR tbuf[1025], tzLic[8192], *pLic=0;
				memset(&tbuf, 0, sizeof(tbuf));
				memset(&tzLic, 0, sizeof(tzLic));

				int nSize=0;
				pLic = tzLic;

				while(nRet = w3.Response(reinterpret_cast<unsigned char *>(tbuf), 1024))
				{
					memcpy(pLic, tbuf, nRet);
					pLic += nRet;
					nSize += nRet;
				};

				// save the licensefile to the USB-memory
				m_csStatus = _T(" Saving license file");
				UpdateData(FALSE);

				CFile cfLicfile;
				if( cfLicfile.Open(m_csPath + _T("HaglofManagementSystems.lic"), CFile::modeCreate|CFile::modeWrite) != 0 )
				{
//					cfLicfile.Write(csBuf, csBuf.GetLength());
					cfLicfile.Write(tzLic, nSize);
					cfLicfile.Close();
				
					m_csStatus = _T(" Done");
					m_bOk = TRUE;
					UpdateData(FALSE);
				}
				else
				{
					m_csStatus = _T(" Could not save license file");
					UpdateData(FALSE);
				}
			}
			else
			{
				m_csStatus.Format(_T(" Server error: %d !"), w3.QueryResult());
				UpdateData(FALSE);
			}
		}
		else
		{
			m_csStatus.Format(_T(" Server error: %d !"), w3.QueryResult());
			UpdateData(FALSE);
		}

		w3.Close();
	}
	else
	{
		m_csStatus.Format(_T(" Server error: %d !"), w3.QueryResult());
	}
	

	GetDlgItem(IDC_LIST_HWID)->EnableWindow(m_bOk);
	GetDlgItem(IDC_BUTTON_COPY_HWID)->EnableWindow(m_bOk);

	GetDlgItem(IDC_EDIT_CODES)->EnableWindow(m_bOk);
	GetDlgItem(IDC_BUTTON_APPLY_CODES)->EnableWindow(m_bOk);

	GetDlgItem(IDC_BUTTON_GETID)->EnableWindow(m_bOk && 0);


	UpdateData(FALSE);
}

BOOL CLicenseDlg::OnInitDialog()
{
	return CDialog::OnInitDialog();
}

// Apply codes to the license file
void CLicenseDlg::OnBnClickedButtonApplyCodes()
{

}

// Copy selected hwid to clipboard
void CLicenseDlg::OnBnClickedButtonCopyHwid()
{
	CString csId;
	m_clbHwids.GetText(m_clbHwids.GetCurSel(), csId);
	csId = csId.Right(8);

	// copy USB hwid to clipboard
	OpenClipboard();
	EmptyClipboard();

	// Allocate a global memory object for the text. 
	HGLOBAL hglbCopy = GlobalAlloc(GMEM_MOVEABLE, (m_csId.GetLength() + 1) * sizeof(TCHAR)); 
	if (hglbCopy == NULL) 
	{ 
		CloseClipboard(); 
		return;
	} 

	// Lock the handle and copy the text to the buffer. 
	LPTSTR lptstrCopy = (LPTSTR)GlobalLock(hglbCopy); 
	memcpy(lptstrCopy, csId.GetBuffer(), csId.GetLength() * sizeof(TCHAR)); 
	lptstrCopy[csId.GetLength()] = (TCHAR) 0;    // null character 
	GlobalUnlock(hglbCopy); 

	SetClipboardData(CF_TEXT, hglbCopy);
	CloseClipboard();
}
