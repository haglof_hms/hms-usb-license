// LicenseDlg2.cpp : implementation file
//

#include "stdafx.h"
#include "License.h"
#include "LicenseDlg2.h"
#include "XBrowseForFolder.h"

// CLicenseDlg2 dialog

IMPLEMENT_DYNAMIC(CLicenseDlg2, CDialog)

CLicenseDlg2::CLicenseDlg2(CWnd* pParent /*=NULL*/)
	: CDialog(CLicenseDlg2::IDD, pParent)
	, m_csLicfilePath(_T(""))
	, m_csHwid(_T(""))
	, m_csStatus(_T(""))
{

}

CLicenseDlg2::~CLicenseDlg2()
{
}

void CLicenseDlg2::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_PATH, m_csLicfilePath);
	DDX_Text(pDX, IDC_EDIT_HWID, m_csHwid);
	DDX_Text(pDX, IDC_STATIC_STATUS, m_csStatus);
	DDX_Control(pDX, IDC_LIST_LICENSES, m_clLicenses);
}


BEGIN_MESSAGE_MAP(CLicenseDlg2, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_DIRCHOOSE, OnBnClickedButtonDirchoose)
	ON_BN_CLICKED(IDC_BUTTON_GETID, OnBnClickedButtonGetid)
	ON_BN_CLICKED(IDC_BUTTON_SHOW_LIC, &CLicenseDlg2::OnBnClickedButtonShowLic)
END_MESSAGE_MAP()


// CLicenseDlg2 message handlers

BOOL CLicenseDlg2::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_clLicenses.InsertColumn(0, _T("ID"));
	m_clLicenses.InsertColumn(1, _T("Description"));
	m_clLicenses.InsertColumn(2, _T("Status"));

	m_clLicenses.SetColumnWidth(0, 50);
	m_clLicenses.SetColumnWidth(1, 120);
	m_clLicenses.SetColumnWidth(2, 260);

	return TRUE;
}

void CLicenseDlg2::OnBnClickedButtonDirchoose()
{
	UpdateData(TRUE);

	// display a directory chooser
	TCHAR szFolder[MAX_PATH*2];
	szFolder[0] = _T('\0');
	BOOL bRet = XBrowseForFolder(NULL,
		m_csLicfilePath,
		szFolder,
		sizeof(szFolder)/sizeof(TCHAR)-2);

	if(bRet == TRUE)
	{
		m_csLicfilePath = szFolder;
		if(m_csLicfilePath.GetAt(m_csLicfilePath.GetLength()-1) != '\\')
			m_csLicfilePath += _T("\\");

		UpdateData(FALSE);
	}
}

void CLicenseDlg2::OnBnClickedButtonGetid()
{
	UpdateData(TRUE);

	m_csStatus = _T(" Getting USB hardware id");
	UpdateData(FALSE);

	// copy a dummy license-file to the usb-memory and open it
	CopyFile(theApp.m_csProgPath + _T("dummy.lic"), m_csLicfilePath + _T("dummy.lic"), FALSE);

	// open license-file
	long lRes = theApp.proxyLicProtector.Prepare(m_csLicfilePath + _T("dummy.lic"), _T("dummy!"));
	if(lRes != 0)
	{
		AfxMessageBox(theApp.proxyLicProtector.GetErrorMessage(lRes), MB_OK);
		m_csStatus = _T(" Unable to open dummy.lic!");
		UpdateData(FALSE);
		return;
	}

	m_csHwid = theApp.proxyLicProtector.GetInstCode(11);
	if(m_csHwid == _T(""))
	{
		m_csHwid = _T("ERROR!");
		m_csStatus = _T(" Could not get USB hardware id!");
		UpdateData(FALSE);
		return;
	}

	theApp.proxyLicProtector.Quit();

	DeleteFile(m_csLicfilePath + _T("dummy.lic"));



	// copy USB hwid to clipboard
	OpenClipboard();
	EmptyClipboard();

	// Allocate a global memory object for the text. 
	HGLOBAL hglbCopy = GlobalAlloc(GMEM_MOVEABLE, (m_csHwid.GetLength() + 1) * sizeof(TCHAR)); 
	if (hglbCopy == NULL) 
	{ 
		CloseClipboard(); 
		return;
	} 

	// Lock the handle and copy the text to the buffer. 
	LPTSTR lptstrCopy = (LPTSTR)GlobalLock(hglbCopy); 
	memcpy(lptstrCopy, m_csHwid.GetBuffer(), m_csHwid.GetLength() * sizeof(TCHAR)); 
	lptstrCopy[m_csHwid.GetLength()] = (TCHAR) 0;    // null character 
	GlobalUnlock(hglbCopy); 

	SetClipboardData(CF_TEXT, hglbCopy);
	CloseClipboard();



	m_csStatus.Format(_T("Done."));
	UpdateData(FALSE);
}

// open up the license file and gather the licenses
void CLicenseDlg2::OnBnClickedButtonShowLic()
{
	CString csBuf, csRes, csType, csStatus, csModules;
	int nCurPos = 0;
	LICENSE lic;
	long lValidate, lRemLic, lRemDays;
	int nLoop = 0;
	BOOL bFirst = FALSE;
	LVITEM lvItem;
	lvItem.mask = LVIF_TEXT; 
	lvItem.state = 0; 
	lvItem.stateMask = 0; 

	m_clLicenses.DeleteAllItems();

	long nRes = theApp.proxyLicProtector.Prepare(m_csLicfilePath + _T("HaglofManagementSystems.lic"), _T("1234567890"));
	if(nRes != 0)
	{
		AfxMessageBox(theApp.proxyLicProtector.GetErrorMessage(nRes), MB_OK);
		AfxMessageBox(theApp.proxyLicProtector.GetErrorMessage(nRes), MB_OK);
		return;
	}

	long lRes = theApp.proxyLicProtector.GetCopyProtection();
	theApp.proxyLicProtector.GetInstCode(lRes);

	csModules = theApp.proxyLicProtector.GetModuleList(false, true);
	csRes = csModules.Tokenize(_T(";"), nCurPos);
	if(csRes != _T("")) csType = csModules.Tokenize(_T(";"), nCurPos);

	while (csRes != _T("") && csType != _T(""))
	{
		if(csType == "U")	// user
			lic.nType = 1;
		else if(csType == "S")	// computer
			lic.nType = 2;
		else if(csType == "R")	// concurrent user
			lic.nType = 6;
		else if(csType == "Y")	// YesNo
			lic.nType = 4;
		else if(csType == "C")	// counter
			lic.nType = 3;
		else if(csType == "I")	// item counter
			lic.nType = 5;

		if(lic.nType == 4)	// YesNo
		{
			lValidate = theApp.proxyLicProtector.ValidatesYes(csRes);

			if(lValidate == 0)
			{
				lic.bYesNo = TRUE;
				lic.bDemo = FALSE;
			}
			else if(lValidate == 1)
			{
				lic.bYesNo = TRUE;
				lic.bDemo = TRUE;
			}
			else if(lValidate == 3)
			{
				lic.bYesNo = FALSE;
				lic.bDemo = FALSE;
			}
		}
		else if(lic.nType == 6)	// concurrent user
		{
			theApp.proxyLicProtector.SetCheckInterval(csRes, 5);

			lValidate = theApp.proxyLicProtector.Validate(csRes, _T(""), true);
			lValidate = theApp.proxyLicProtector.Validate(csRes, _T(""), true);
			lRemLic = theApp.proxyLicProtector.RemainingLicences(csRes);


			if(lValidate == 1)	// demo version
				lic.bDemo = TRUE;
			else if(lValidate == 0)	// no demo
				lic.bDemo = FALSE;
			else
			{
//				lic.bDemo = TRUE;

				// concurrent users
				if(lValidate == 32)	// no free licenses
				{
					long lLicenses = theApp.proxyLicProtector.TotalLicences(csRes);
					if(lLicenses == 1)	// we have only 1 license, and its in use
					{
						// try if it is a dead client
						theApp.proxyLicProtector.RemoveAllItems(csRes);
/*						csBuf2 = theApp.proxyLicProtector.GetEntriesOfModule(csRes, true, ";");
						theApp.proxyLicProtector.DeactivateEntry(csRes, csBuf2);
						theApp.proxyLicProtector.RemoveDeactivated(csRes);*/

						if(bFirst == TRUE)
						{
//							bFirst = FALSE;
							long lInterval = theApp.proxyLicProtector.CheckInterval(csRes);
							Sleep(lInterval * 2000);
						}

						// test again
						lValidate = theApp.proxyLicProtector.Validate(csRes, _T(""), true);

						if(lValidate == 0)	// it was a dead user
						{
							lic.bDemo = FALSE;
							lRemLic = 0;
						}
						else if(lValidate == 1)	// demo
						{
							lic.bDemo = TRUE;
							lRemLic = 0;
						}
						else	// license still in use
						{
							lic.bDemo = FALSE;
							lRemLic = -1;
						}
					}
				}
			}
		}
		else
		{
			lValidate = theApp.proxyLicProtector.Validate(csRes, _T(""), false);

			if(lValidate == 1)	// demo version
				lic.bDemo = TRUE;
			else if(lValidate == 0)	// no demo
				lic.bDemo = FALSE;
			else
				lic.bDemo = TRUE;
		}

		lic.nNoOfDays = theApp.proxyLicProtector.NoOfDays(csRes);
		lic.nWebActivation = theApp.proxyLicProtector.GetWebActivation(csRes);
		lic.dtExpires = theApp.proxyLicProtector.ExpiredOn(csRes);
		lRemDays = theApp.proxyLicProtector.RemainingDays(csRes);
		lic.nRemDays = (int)lRemDays;
		if(lic.nType != 6) lRemLic = theApp.proxyLicProtector.RemainingLicences(csRes);
		lic.nRemLic = (int)lRemLic;
		lic.csModID = csRes;
		lic.csModName = theApp.proxyLicProtector.ModuleName(lic.csModID);
		lic.csTag = theApp.proxyLicProtector.GetTagValue(lic.csModID);


		// add license to the report control
		if(lic.bDemo == TRUE)
		{
			csStatus = _T("Demo.");

			if(lic.nRemDays == 0 && lic.nRemLic == 0)	// expired
			{
				csStatus += _T(" Evaluation expired.");
			}
			else 
			{
				if(lic.nRemDays > 0)	// days remaining
				{
					csBuf.Format(_T(" %d day(s) remaining."), lic.nRemDays);
					csStatus += _T(" ") + csBuf;
				}

				if(lic.nRemLic > 0)	// item remaining
				{
					if(lic.nType != 6)
					{
						csBuf.Format(_T(" %d run(s) remaining."), lic.nRemLic);
						csStatus += _T(" ") + csBuf;
					}
				}
			}
		}
		else
		{
			if(lic.nType == 6)	// concurrent user
			{
				if(lic.nRemLic == -1)	//if(lic.nRemLic == -1)
				{
					csStatus = _T("Locked");
				}
				else
				{
					csStatus = _T("Active");
					csBuf.Format(_T(" %d license(s) available."), lic.nRemLic);
					csStatus += _T(" ") + csBuf;
				}
			}
			else
			{
				csStatus.Format(_T("Module active."));
			}
		}


		// add record
		lvItem.iItem = nLoop;

		lvItem.iSubItem = 0;
		lvItem.pszText = lic.csModID.GetBuffer(0);
		m_clLicenses.InsertItem(&lvItem);

		lvItem.iSubItem = 1;
		lvItem.pszText = lic.csModName.GetBuffer(0);
		m_clLicenses.SetItem(&lvItem);

		lvItem.iSubItem = 2;
		lvItem.pszText = csStatus.GetBuffer(0);
		m_clLicenses.SetItem(&lvItem);



		// get next module
		csRes = csModules.Tokenize(_T(";"), nCurPos);
		if(csRes != _T("")) csType = csModules.Tokenize(_T(";"), nCurPos);
		nLoop++;
	};

	theApp.proxyLicProtector.Quit();
}