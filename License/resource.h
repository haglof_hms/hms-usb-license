//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by License.rc
//
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     129
#define IDD_DIALOG2                     130
#define IDD_DIALOG3                     131
#define IDC_EDIT1                       1002
#define IDC_EDIT_PATH                   1002
#define IDC_BUTTON_DIRCHOOSE            1003
#define IDC_BUTTON_GETID                1004
#define IDC_EDIT_ID                     1005
#define IDC_STATIC_STATUS               1006
#define IDC_EDIT_CODES                  1007
#define IDC_BUTTON_APPLY_CODES          1008
#define IDC_LIST_HWID                   1009
#define IDC_BUTTON_COPY_HWID            1010
#define IDC_EDIT_HWID                   1011
#define IDC_LIST_LICENSES               1012
#define IDC_BUTTON_SHOW_LIC             1013
#define IDC_LIST2                       1014
#define IDC_EDIT2                       1015
#define IDC_BUTTON1                     1016

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1017
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
