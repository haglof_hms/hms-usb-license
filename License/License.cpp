// License.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "License.h"
#include "LicenseDlg.h"
#include "LicenseDlg2.h"
#include "LicenseDlg3.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CLicenseApp

BEGIN_MESSAGE_MAP(CLicenseApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CLicenseApp construction

CLicenseApp::CLicenseApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CLicenseApp object

CLicenseApp theApp;


// CLicenseApp initialization

BOOL CLicenseApp::InitInstance()
{
	if (!AfxOleInit())
	{
		return FALSE;
	}
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Haglof"));
	m_csDestPath = GetProfileString(_T("Settings"), _T("DestPath"), _T(""));
	if(m_csDestPath.Right(1) != _T("\\")) m_csDestPath += _T("\\");

	// Create the automation object
	BOOL bRes = proxyLicProtector.CreateDispatch(_T("LicProtectorEasyGo.LicProtectorEasyGo270"));
	if(!bRes)
	{
		AfxMessageBox(_T("Automation object not found!"), MB_OK);
		return FALSE;
	}

	// get the programpath
	CString csBuf = m_pszHelpFilePath;
#ifndef UNICODE
	m_csProgPath = csBuf.Left(csBuf.GetLength() - strlen(m_pszExeName) - 4);
#else
	m_csProgPath = csBuf.Left(csBuf.GetLength() - wcslen(m_pszExeName) - 4);
#endif

//	CLicenseDlg3 dlg;
//	dlg.m_csPath = m_csDestPath;

	CLicenseDlg2 dlg;
	dlg.m_csLicfilePath = m_csDestPath;

	dlg.DoModal();


	proxyLicProtector.Quit();
	proxyLicProtector.ReleaseDispatch();

	// save the path to the registry
//	WriteProfileString(_T("Settings"), _T("DestPath"), dlg.m_csPath);
	WriteProfileString(_T("Settings"), _T("DestPath"), dlg.m_csLicfilePath);


	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
