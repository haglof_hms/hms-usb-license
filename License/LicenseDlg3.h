#pragma once


// CLicenseDlg3 dialog

class CLicenseDlg3 : public CDialog
{
	DECLARE_DYNAMIC(CLicenseDlg3)

public:
	CLicenseDlg3(CWnd* pParent = NULL);   // standard constructor
	virtual ~CLicenseDlg3();

// Dialog Data
	enum { IDD = IDD_DIALOG3 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CString m_csPath;
	CString m_csKey;
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButtonDirchoose();
};
